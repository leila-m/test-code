var gulp = require('gulp'),
    less = require('gulp-less'),
    notify = require('gulp-notify'),
    gutil = require('gulp-util'),
    themeFolder = '',
    minify = require('gulp-minify');
    sass = require('gulp-sass'),
    sassSrcFolder = 'scss/';
    jsSrcFolder = 'scripts/';
    uglify = require('gulp-uglify');

gulp.task('sass', function() {
  return gulp.src(sassSrcFolder + '*.scss')
.pipe(sass())
    .on('error', sass.logError)
  .pipe(gulp.dest(themeFolder))
  .pipe(notify({ title: 'Styles Complete.', message: " " }))
});



gulp.task('watch', function() {
  // Watch .scss files
  gulp.watch([
    sassSrcFolder + '*.scss'
    ], ['styles']);
});

gulp.task('compress', function() {
  gulp.src(themeFolder + jsSrcFolder + '*.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('scripts/min'))
});
gulp.task('styles', ['sass']);
gulp.task('default', ['styles', 'watch', 'compress']);